import tornado.ioloop
import tornado.web
import sqlite3
import os.path
import logging
import tornado.escape
import os.path
import uuid

from tornado.concurrent import Future
from tornado import gen
from tornado.options import define, options, parse_command_line

def _execute(query):
        dbPath = 'new.db'
        connection = sqlite3.connect(dbPath)
        cursorobj = connection.cursor()
        try:
                cursorobj.execute(query)
                result = cursorobj.fetchall()
                connection.commit()
        except Exception:
                raise
        connection.close()
        return result


class Main(tornado.web.RequestHandler):
    def get(self):
        self.write("Main")

class AddStudent(tornado.web.RequestHandler):
    def get(self):
        self.render('sqliteform.html')

    def post(self):
        password = self.get_argument("password")
        name = self.get_argument("name")
        query = '''SELECT name FROM stu WHERE name=%s AND password=%s''' %(name, password);
        _execute(query)
        self.render('success.html')

class LoginStudent(tornado.web.RequestHandler):
    def get(self):
        self.render('sqlitelogin.html')

    def post(self):
        password = self.get_argument("password")
        name = self.get_argument("name")
        query = ''' select * from stud where name= ''' + name
        row = _execute(query)
        if password == str(row[2]):
            self.render('success_login.html')

class ShowStudents(tornado.web.RequestHandler):
    def get(self):
        query = ''' select * from stud'''
        rows = _execute(query)
        self._processresponse(rows)

    def _processresponse(self,rows):
        self.write("<b>Records</b> <br /><br />")
        for row in rows:
                self.write(str(row[0]) + "      " + str(row[1]) + "     " + str(row[2]) +" <br />" )
application = tornado.web.Application([
    (r"/", Main),
    (r"/create" ,AddStudent),
    (r"/login" ,LoginStudent),
    (r"/show",ShowStudents),
],	static_path=os.path.join(os.path.dirname(__file__), "static"),
	debug=True,
)

if __name__ == "__main__":
    application.listen(8866)
    tornado.ioloop.IOLoop.instance().start()
